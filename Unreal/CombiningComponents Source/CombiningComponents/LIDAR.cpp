// Fill out your copyright notice in the Description page of Project Settings.

#include "LIDAR.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
ULIDAR::ULIDAR()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void ULIDAR::BeginPlay()
{
	Super::BeginPlay();
	WorldContext = GetWorld();
	// ...
	
}


// Called every frame
void ULIDAR::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (currentTick % 50)
	{
		LIDARScan(270, 2);
	}
	currentTick++;

	// ...
}

void ULIDAR::LIDARScan(float measurementAngle, float angularResolution)
{
	FVector StartScanVector = GetOwner()->GetActorForwardVector();

	for (float scanAngle = -(measurementAngle / 2); scanAngle <= measurementAngle / 2; scanAngle += angularResolution)
	{
		singleLineScan(scanAngle);
	}
}

void ULIDAR::singleLineScan(float angle)
{
	//Vertical Offset
	FVector offset = FVector(0, 0, 15.f);

	// Check to see if we are getting the world context
	//UWorld* WorldContext = GetWorld();
	if (!WorldContext)
	{
		//UE_LOG(LogTemp, Warning, TEXT("NO World Context Available!"));
		return;
	}

	//    Holds information about what was hit, distance, etc
	FHitResult* HitResult = new FHitResult();

	//FVector StartLocation = GetOwner()->GetActorLocation();
	FVector StartLocation = GetOwner()->GetActorLocation() + offset;
	UE_LOG(LogTemp, Warning, TEXT("My Character's Location is %s"), *StartLocation.ToCompactString());

	FVector ForwardVector = GetOwner()->GetActorForwardVector().RotateAngleAxis(angle, FVector(0, 0, 1));
	FVector End = ((ForwardVector * 1000.f) + StartLocation);
	UE_LOG(LogTemp, Warning, TEXT("Tracing Ends at %s"), *End.ToCompactString());
	FCollisionQueryParams* TraceParams = new FCollisionQueryParams();
	TraceParams->AddIgnoredActor(GetOwner()->GetUniqueID());
	if (GetOwner())
	{
		UE_LOG(LogTemp, Warning, TEXT("We have an owner"));
		//TraceParams->AddIgnoredActor(GetOwner()->GetUniqueID());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("We have no owner"));
	}
	//TraceParams->AddIgnoredActor(GetOwner()->GetUniqueID());

	if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartLocation, End, ECC_Visibility, *TraceParams))
	{
		//UE_LOG(LogTemp, Warning, TEXT("HITTING SOMETHING!"));
		End = ((ForwardVector * HitResult->Distance) + StartLocation);
		FVector StartPoint = End - ForwardVector * 5.f;
		//DrawDebugLine(GetWorld(), StartLocation, End, FColor(255, 0, 0), false, 0.0f, 0.0f, 1.0f);
		DrawDebugLine(GetWorld(), StartPoint, End, FColor(255, 0, 0), false, 0.0f, 0.0f, 5.0f);
	}
	else
	{
		FVector StartPoint = End - ForwardVector * 5.f;
		//DrawDebugLine(GetWorld(), StartLocation, End, FColor(0, 255, 0), false, 0.0f, 0.0f, 1.0f);
		DrawDebugLine(GetWorld(), StartPoint, End, FColor(0, 255, 0), false, 0.0f, 0.0f, 5.0f);
	}
}
