// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#pragma push_macro("CONSTEXPR")
#undef CONSTEXPR
#pragma push_macro("dynamic_cast")
#undef dynamic_cast
#pragma push_macro("check")
#undef check
#pragma push_macro("PI")
#undef PI
#include "ROS2ImagePublisher.h"
#pragma pop_macro("PI")
#pragma pop_macro("check")
#pragma pop_macro("dynamic_cast")
#pragma pop_macro("CONSTEXPR")

#include "WheeledVehicle.h"
#include "WheeledVehicleMovementComponent.h"
#include "WheeledVehicleMovementComponent4W.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LIDARActor.generated.h"

UCLASS()
class COMBININGCOMPONENTS_API ALIDARActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALIDARActor();
	ROSLaserScanPublisher *laserPub;
	double angle_min;
	double angle_max;
	double angle_increment;
	double range_min;
	double range_max;
	int currentTick = 0;

	float GetSteering(float Value);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void visualSingleLineScan(float angle);
	void visualLIDARScan(float measurementAngle, float angularResolution);
	double singleLineScan(float angle);
	void LIDARScan(double ranges[]);
	UWorld* WorldContext;
	
};
