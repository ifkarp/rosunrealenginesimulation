// Fill out your copyright notice in the Description page of Project Settings.

#include "OutputToOpenCVTest.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Camera/CameraComponent.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Engine.h"
#include "CombiningComponents.h"



// Sets default values
AOutputToOpenCVTest::AOutputToOpenCVTest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RightCamera = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("RightCamera"));
	RightCamera->SetupAttachment(RootComponent);
	RightCamera->SetRelativeLocation(FVector(0, 50, 0));

	textureTarget = NewObject<UTextureRenderTarget2D>();
	textureTarget->bHDR_DEPRECATED = false;
	textureTarget->ClearColor = FLinearColor::Red;
	textureTarget->InitAutoFormat(512, 512);

	RightCamera->TextureTarget = textureTarget;

	//rt_resource = RightCamera->TextureTarget->GetRenderTargetResource();
}

// Called when the game starts or when spawned
void AOutputToOpenCVTest::BeginPlay()
{
	Super::BeginPlay();
	//OutputImageToOpenCV(RightCamera->TextureTarget);
	//RightCamera->CaptureScene();
}

// Called every frame
void AOutputToOpenCVTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//createAndDisplayImage();

}

void AOutputToOpenCVTest::createAndDisplayImage()
{
	RightCamera->CaptureScene();

	cv::Mat mat(100, 100, CV_64FC1);
	double mean = 0.0;
	double stddev = 500.0 / 3.0;
	cv::randn(mat, cv::Scalar(mean), cv::Scalar(stddev));

	cv::imshow("Display Window", mat);
}

void AOutputToOpenCVTest::OutputImageToOpenCV(UTextureRenderTarget2D *texture2DRenderTarget)
{
	TArray<FColor> TexturePixels;
	FTextureRenderTargetResource * fTextureRenderTargetRes = texture2DRenderTarget->GameThread_GetRenderTargetResource();
	int TexWidth = texture2DRenderTarget->SizeX;
	int TexHeight = texture2DRenderTarget->SizeY;
	int ImageSize = TexWidth * TexHeight;
	TexturePixels.AddUninitialized(ImageSize);

	//cv::Mat mat(TexHeight, TexWidth, CV_8UC3, cv::Scalar(0, 0, 0));

	fTextureRenderTargetRes->ReadPixels(TexturePixels);
	
	//if (bReadSuccess)
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("SUCCESS"));
	//}
	//else
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("Failed"));
	//}
	//cv::imshow("Display Window", mat);
}

