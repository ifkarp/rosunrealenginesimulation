// Fill out your copyright notice in the Description page of Project Settings.

#include "LIDARActor.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"


// Sets default values
ALIDARActor::ALIDARActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALIDARActor::BeginPlay()
{
	Super::BeginPlay();
	angle_min = -135;
	angle_max = 135;
	angle_increment = 2;
	range_min = 2;
	range_max = 1000;

	laserPub = new ROSLaserScanPublisher(angle_min, angle_max, angle_increment, range_min, range_max);

	//Need GetWorld when finding if a line trace intersects another object in the world
	WorldContext = GetWorld();
	if (!WorldContext)
	{
		//UE_LOG(LogTemp, Warning, TEXT("NO World Context Available!"));
	}
}

float ALIDARActor::GetSteering(float Value)
{
	return Value;
}

// Called every frame
void ALIDARActor::Tick(float DeltaTime)
{
	currentTick++;
	Super::Tick(DeltaTime);
	int totalRanges = (angle_max - angle_min) / angle_increment;
	int input = 0;
	double ranges[135]; //Should be (angle_max - angle_min) / angle_increment
	ALIDARActor::LIDARScan(ranges);
	//double steering = ((AWheeledVehicle*)GetOwner())->GetVehicleMovementComponent()->Steer;
	//auto steering = GetWorld()->GetFirstPlayerController()->GetInputAxisKeyValue(FKey);
	//auto owner = GetOwner();
	//UE_LOG(LogTemp, Warning, TEXT("Steering: %f"),steering);
	laserPub->PublishScan(ranges, 0.5);
	//Used to restart the level, will be moved later
	//if (currentTick >= 100)
	//{
	//	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	//}
}

void ALIDARActor::visualLIDARScan(float measurementAngle, float angularResolution)
{
	FVector StartScanVector = GetActorForwardVector();

	for (float scanAngle = -(measurementAngle / 2); scanAngle <= measurementAngle / 2; scanAngle += angularResolution)
	{
		visualSingleLineScan(scanAngle);
	}
}

void ALIDARActor::visualSingleLineScan(float angle)
{
	FHitResult* HitResult = new FHitResult();

	
	FVector StartLocation = GetActorLocation();
	FVector ForwardVector = GetActorForwardVector().RotateAngleAxis(angle, GetActorUpVector());
	//FVector ForwardVector = GetActorForwardVector().RotateAngleAxis(angle, FVector(0, 0, 1));
	FVector End = ((ForwardVector * 1000.f) + StartLocation);
	FCollisionQueryParams* TraceParams = new FCollisionQueryParams();
	TraceParams->AddIgnoredActor(GetUniqueID());

	if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartLocation, End, ECC_Visibility, *TraceParams))
	{
		//UE_LOG(LogTemp, Warning, TEXT("HITTING SOMETHING!"));
		End = ((ForwardVector * HitResult->Distance) + StartLocation);
		FVector StartPoint = End - ForwardVector * 5.f; //Used if you want the LIDAR to show points where it meets an obstacle
		//DrawDebugLine(GetWorld(), StartLocation, End, FColor(255, 0, 0), false, 0.0f, 0.0f, 1.0f);
		DrawDebugLine(GetWorld(), StartPoint, End, FColor(255, 0, 0), false, 0.0f, 0.0f, 5.0f);
	}
	else
	{
		FVector StartPoint = End - ForwardVector * 5.f; //Used if you want the LIDAR to show points at the limits of its range
		//DrawDebugLine(GetWorld(), StartLocation, End, FColor(0, 255, 0), false, 0.0f, 0.0f, 1.0f);
		DrawDebugLine(GetWorld(), StartPoint, End, FColor(0, 255, 0), false, 0.0f, 0.0f, 5.0f);
	}
}

double ALIDARActor::singleLineScan(float angle)
{
	FHitResult* HitResult = new FHitResult();

	FVector StartLocation = GetActorLocation();
	FVector ForwardVector = GetActorForwardVector().RotateAngleAxis(angle, GetActorUpVector());
	//FVector ForwardVector = GetActorForwardVector().RotateAngleAxis(angle, FVector(0, 0, 1));
	FVector End = ((ForwardVector * 1000.f) + StartLocation);
	FCollisionQueryParams* TraceParams = new FCollisionQueryParams();
	TraceParams->AddIgnoredActor(GetUniqueID());

	if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartLocation, End, ECC_Visibility, *TraceParams))
	{
		//UE_LOG(LogTemp, Warning, TEXT("HITTING SOMETHING!"));
		End = ((ForwardVector * HitResult->Distance) + StartLocation);
		FVector StartPoint = End - ForwardVector * 5.f; //Used if you want the LIDAR to show points where it meets an obstacle
														//DrawDebugLine(GetWorld(), StartLocation, End, FColor(255, 0, 0), false, 0.0f, 0.0f, 1.0f);
		DrawDebugLine(GetWorld(), StartPoint, End, FColor(255, 0, 0), false, 0.0f, 0.0f, 5.0f);
		if (HitResult->Distance < range_min)
		{
			return range_min;
		}
		else
		{
			return HitResult->Distance;
		}
	}
	else
	{
		FVector StartPoint = End - ForwardVector * 5.f; //Used if you want the LIDAR to show points at the limits of its range
														//DrawDebugLine(GetWorld(), StartLocation, End, FColor(0, 255, 0), false, 0.0f, 0.0f, 1.0f);
		DrawDebugLine(GetWorld(), StartPoint, End, FColor(0, 255, 0), false, 0.0f, 0.0f, 5.0f);
		return range_max;
	}
}

void ALIDARActor::LIDARScan(double ranges[])
{
	FVector StartScanVector = GetActorForwardVector();
	int currentScan = 0;
	for (float scanAngle = angle_min; scanAngle <= angle_max; scanAngle += angle_increment)
	{
		ranges[currentScan] = singleLineScan(scanAngle);
		currentScan++;
	}
}

