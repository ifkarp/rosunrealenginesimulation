// Fill out your copyright notice in the Description page of Project Settings.

#include "StereoCameras.h"
#include "Camera/CameraComponent.h"


// Sets default values
AStereoCameras::AStereoCameras()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LeftCameraBase = CreateDefaultSubobject<USceneComponent>(TEXT("LeftCameraBase"));
	LeftCameraBase->SetupAttachment(RootComponent);

	LeftCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("LeftCamera"));
	LeftCamera->SetupAttachment(LeftCameraBase);
	LeftCamera->SetRelativeLocation(FVector(0, -50, 0));

	RightCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("RightCamera"));
	RightCamera->SetupAttachment(LeftCameraBase);
	RightCamera->SetRelativeLocation(FVector(0, 50, 0));
}

// Called when the game starts or when spawned
void AStereoCameras::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStereoCameras::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

