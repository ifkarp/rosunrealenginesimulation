// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#pragma push_macro("CONSTEXPR")
#undef CONSTEXPR
#pragma push_macro("dynamic_cast")
#undef dynamic_cast
#pragma push_macro("check")
#undef check
#pragma push_macro("PI")
#undef PI
#include "ROS2ImagePublisher.h"
#pragma pop_macro("PI")
#pragma pop_macro("check")
#pragma pop_macro("dynamic_cast")
#pragma pop_macro("CONSTEXPR")


#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"	
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

#include "Components/SceneCaptureComponent2D.h"
#include "Camera/CameraActor.h"
#include "Engine/TextureRenderTarget2D.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AttemptWithROSPublisher1.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class COMBININGCOMPONENTS_API UAttemptWithROSPublisher1 : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAttemptWithROSPublisher1();
	~UAttemptWithROSPublisher1();
	ROSPublisher *publisher;
	USceneCaptureComponent2D * camera = nullptr;
	/*char *argv[] = { "program name", "arg1", "arg2", NULL };
	int argc = sizeof(argv) / sizeof(char*) - 1;*/
	int currentTick = 0;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;



public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	//static void TextureToOpenCVMatrix(UTextureRenderTarget2D *texture2DRenderTarget);

		
	
};
