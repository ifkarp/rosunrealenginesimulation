// Fill out your copyright notice in the Description page of Project Settings.

#include "VelodyneScanner.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"

// Sets default values
AVelodyneScanner::AVelodyneScanner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AVelodyneScanner::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AVelodyneScanner::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

	WorldContext = GetWorld();
	if (!WorldContext)
	{
		UE_LOG(LogTemp, Warning, TEXT("NO World Context Available!"));
	}

	horizontalScan(-40, 40, 4);
}

void AVelodyneScanner::horizontalScan(float minAngle, float maxAngle, float angularResolution)
{
	for (float scanPlane = minAngle; scanPlane <= maxAngle; scanPlane += angularResolution)
	{
		singleBeamScan(GetActorForwardVector().RotateAngleAxis(scanPlane, GetActorUpVector()), -30, 10, angularResolution);
	}
}

void AVelodyneScanner::singleBeamScan(FVector scanPlane, float minAngle, float maxAngle, float angularResolution)
{
	for (float scanAngle = minAngle; scanAngle <= maxAngle; scanAngle += angularResolution)
	{
		singleLineScan(scanPlane.RotateAngleAxis(scanAngle, GetActorRightVector()));
	}
}

void AVelodyneScanner::singleLineScan(FVector scanLine)
{
	FHitResult* HitResult = new FHitResult();

	FVector StartLocation = GetActorLocation();
	FVector ForwardVector = scanLine;
	//FVector ForwardVector = GetActorForwardVector().RotateAngleAxis(angle, FVector(0, 0, 1));
	FVector End = ((ForwardVector * 500.f) + StartLocation);
	FCollisionQueryParams* TraceParams = new FCollisionQueryParams();
	TraceParams->AddIgnoredActor(GetUniqueID());

	if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartLocation, End, ECC_Visibility, *TraceParams))
	{
		//UE_LOG(LogTemp, Warning, TEXT("HITTING SOMETHING!"));
		End = ((ForwardVector * HitResult->Distance) + StartLocation);
		FVector StartPoint = End - ForwardVector * 7.5f; //Used if you want the LIDAR to show points where it meets an obstacle
														//DrawDebugLine(GetWorld(), StartLocation, End, FColor(255, 0, 0), false, 0.0f, 0.0f, 1.0f);
		DrawDebugLine(GetWorld(), StartPoint, End, FColor(255, 0, 0), false, 0.0f, 0.0f, 7.5f);
	}
	else
	{
		FVector StartPoint = End - ForwardVector * 7.5f; //Used if you want the LIDAR to show points at the limits of its range
														//DrawDebugLine(GetWorld(), StartLocation, End, FColor(0, 255, 0), false, 0.0f, 0.0f, 1.0f);
		DrawDebugLine(GetWorld(), StartPoint, End, FColor(0, 255, 0), false, 0.0f, 0.0f, 7.5f);
	}
}

