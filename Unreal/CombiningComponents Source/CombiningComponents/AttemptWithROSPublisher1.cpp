// Fill out your copyright notice in the Description page of Project Settings.

#include "AttemptWithROSPublisher1.h"
#include <iostream>

// Sets default values for this component's properties
UAttemptWithROSPublisher1::UAttemptWithROSPublisher1()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

UAttemptWithROSPublisher1::~UAttemptWithROSPublisher1()
{
	//This usually breaks Unreal when you try to close it, compile, or add new C++ classes after hitting Play once
	//publisher->~ROSPublisher();
	UE_LOG(LogTemp, Warning, TEXT("Your message"));
}

// Called when the game starts
void UAttemptWithROSPublisher1::BeginPlay()
{
	Super::BeginPlay();
	//Had to make my own inputs or rclcpp::init(argc, argv) won't work. Didn't experiment much with argv and argc since this random input worked right away
	char *argv[] = { "program name", "arg1", "arg2", NULL };
	int argc = sizeof(argv) / sizeof(char*) - 1;

	publisher = new ROSPublisher(argc, argv);
	//publisher.PublishTest();

	AActor* owner = GetOwner();
	TArray<USceneCaptureComponent2D*> components;
	//owner->GetComponentsByClass<USceneCaptureComponent2D>(components);
	owner->GetComponents(components);

	for (USceneCaptureComponent2D* comp : components)
	{
		camera = comp;
	}
	
}


// Called every frame
void UAttemptWithROSPublisher1::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Will publish after every publishRate ticks
	int publishRate = 10;
	if (currentTick % publishRate == 0)
	{

		TArray<FColor> TexturePixels;
		/*USceneCaptureComponent2D *owner = GetOwner();*/
		UTextureRenderTarget2D *texture2DRenderTarget = camera->TextureTarget;
		FTextureRenderTargetResource * fTextureRenderTargetRes = texture2DRenderTarget->GameThread_GetRenderTargetResource();
		//FTextureRenderTargetResource * fTextureRenderTargetRes = GetOwner()->UTextureRenderTarget2D->GameThread_GetRenderTargetResource();
		int TexWidth = texture2DRenderTarget->SizeX;
		int TexHeight = texture2DRenderTarget->SizeY;
		int ImageSize = TexWidth * TexHeight;
		TexturePixels.AddUninitialized(ImageSize);

		cv::Mat mat(TexHeight, TexWidth, CV_8UC3);

		fTextureRenderTargetRes->ReadPixels(TexturePixels);
		int index = 0;
		for (int i = 0; i < TexHeight; ++i)
		{
			cv::Vec3b* pixel = mat.ptr<cv::Vec3b>(i);

			for (int j = 0; j < TexWidth; ++j)
			{
				index = i*TexWidth + j;
				pixel[j][2] = TexturePixels[index].R;
				pixel[j][1] = TexturePixels[index].G;
				pixel[j][0] = TexturePixels[index].B;
			}
		}


		publisher->Publish(mat, true);
	}
	currentTick++;
}

