// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VelodyneScanner.generated.h"

UCLASS()
class COMBININGCOMPONENTS_API AVelodyneScanner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVelodyneScanner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void singleLineScan(FVector scanLine);
	void singleBeamScan(FVector scanPlane, float minAngle, float maxAngle, float angularResolution);
	void horizontalScan(float minAngle, float maxAngle, float angularResolution);
	UWorld* WorldContext;
	
	
};
