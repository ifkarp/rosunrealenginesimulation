// Fill out your copyright notice in the Description page of Project Settings.
// Used https://wiki.unrealengine.com/Integrating_OpenCV_Into_Unreal_Engine_4 to link OpenCV
//https://answers.unrealengine.com/questions/718575/error-when-updating-to-418-generating-visual-studi.html to attempt to fix errors

using UnrealBuildTool;
using System.IO;

public class CombiningComponents : ModuleRules
{
    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/"));  }
    }

    public bool LoadROSPublisher(ReadOnlyTargetRules Target)
    {
        // Start OpenCV linking here!
        bool isLibrarySupported = false;

        // Create OpenCV Path 
        string ROSPublisherPath = Path.Combine(ThirdPartyPath, "ROSPublisher");

        // Get Library Path 
        string LibPath = "";
        bool isdebug = Target.Configuration == UnrealTargetConfiguration.Debug && BuildConfiguration.bDebugBuildsActuallyUseDebugCRT;
        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            LibPath = Path.Combine(ROSPublisherPath, "Libraries");
            isLibrarySupported = true;
        }
        else
        {
            string Err = string.Format("{0} dedicated server is made to depend on {1}. We want to avoid this, please correct module dependencies.", Target.Platform.ToString(), this.ToString()); System.Console.WriteLine(Err);
        }

        if (isLibrarySupported)
        {
            //Add Include path 
            //PublicIncludePaths.AddRange(new string[] { Path.Combine(OpenCVPath, "Includes") });

            PublicIncludePaths.AddRange(new string[] { @"C:\opencv\include" });
            PublicIncludePaths.AddRange(new string[] { @"C:\dev\ros2\install\include" });
            PublicIncludePaths.AddRange(new string[] { @"C:\Users\unreal\Documents\Visual Studio 2015\Projects\ROS2ImagePublisher\ROS2ImagePublisher" });

            // Add Library Path 
            PublicLibraryPaths.Add(LibPath);

            //Add Static Libraries
            PublicAdditionalLibraries.Add("ROS2ImagePublisher.lib");

            //Add Dynamic Libraries
            PublicDelayLoadDLLs.Add("visualization_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("visualization_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("visualization_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("visualization_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("visualization_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("actionlib_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("actionlib_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("actionlib_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("actionlib_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("actionlib_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("ament_index_cpp.dll");
            PublicDelayLoadDLLs.Add("builtin_interfaces__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("builtin_interfaces__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("builtin_interfaces__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("builtin_interfaces__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("builtin_interfaces__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("class_loader.dll");
            PublicDelayLoadDLLs.Add("client_component.dll");
            PublicDelayLoadDLLs.Add("composition__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("composition__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("composition__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("composition__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("composition__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("composition_nodes.dll");
            PublicDelayLoadDLLs.Add("concrt140.dll");
            PublicDelayLoadDLLs.Add("console_bridge.dll");
            PublicDelayLoadDLLs.Add("diagnostic_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("diagnostic_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("diagnostic_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("diagnostic_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("diagnostic_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("example_interfaces__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("example_interfaces__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("example_interfaces__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("example_interfaces__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("example_interfaces__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("fastcdr-1.0.dll");
            PublicDelayLoadDLLs.Add("fastrtps-1.5.dll");
            PublicDelayLoadDLLs.Add("geometry_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("geometry_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("geometry_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("geometry_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("geometry_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("image_geometry.dll");
            PublicDelayLoadDLLs.Add("kdl_parser.dll");
            PublicDelayLoadDLLs.Add("lifecycle_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("lifecycle_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("lifecycle_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("lifecycle_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("lifecycle_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("listener_component.dll");
            PublicDelayLoadDLLs.Add("msvcp140.dll");
            PublicDelayLoadDLLs.Add("nav_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("nav_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("nav_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("nav_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("nav_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("pendulum_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("pendulum_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("pendulum_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("pendulum_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("pendulum_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("PocoFoundation.dll");
            PublicDelayLoadDLLs.Add("rcl.dll");
            PublicDelayLoadDLLs.Add("rcl_interfaces__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("rcl_interfaces__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("rcl_interfaces__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("rcl_interfaces__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("rcl_interfaces__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("rcl_lifecycle.dll");
            PublicDelayLoadDLLs.Add("rclcpp.dll");
            PublicDelayLoadDLLs.Add("rclcpp_lifecycle.dll");
            PublicDelayLoadDLLs.Add("rcutils.dll");
            PublicDelayLoadDLLs.Add("rmw.dll");
            PublicDelayLoadDLLs.Add("rmw_fastrtps_cpp.dll");
            PublicDelayLoadDLLs.Add("ROS2ImagePublisher.dll");
            PublicDelayLoadDLLs.Add("rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("sensor_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("sensor_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("sensor_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("sensor_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("sensor_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("server_component.dll");
            PublicDelayLoadDLLs.Add("shape_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("shape_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("shape_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("shape_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("shape_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("std_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("std_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("std_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("std_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("std_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("std_srvs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("std_srvs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("std_srvs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("std_srvs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("std_srvs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("stereo_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("stereo_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("stereo_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("stereo_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("stereo_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("talker_component.dll");
            PublicDelayLoadDLLs.Add("test_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("test_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("test_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("test_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("test_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("tf2.dll");
            PublicDelayLoadDLLs.Add("tf2_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("tf2_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("tf2_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("tf2_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("tf2_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("tf2_ros.dll");
            PublicDelayLoadDLLs.Add("trajectory_msgs__rosidl_generator_c.dll");
            PublicDelayLoadDLLs.Add("trajectory_msgs__rosidl_typesupport_c.dll");
            PublicDelayLoadDLLs.Add("trajectory_msgs__rosidl_typesupport_cpp.dll");
            PublicDelayLoadDLLs.Add("trajectory_msgs__rosidl_typesupport_introspection_c.dll");
            PublicDelayLoadDLLs.Add("trajectory_msgs__rosidl_typesupport_introspection_cpp.dll");
            PublicDelayLoadDLLs.Add("urdf.dll");
            PublicDelayLoadDLLs.Add("urdfdom_model.dll");
            PublicDelayLoadDLLs.Add("urdfdom_model_state.dll");
            PublicDelayLoadDLLs.Add("urdfdom_sensor.dll");
            PublicDelayLoadDLLs.Add("urdfdom_world.dll");
            PublicDelayLoadDLLs.Add("vcruntime140.dll");
        }

        Definitions.Add(string.Format("WITH_ROSPUBLISHER_BINDING={0}", isLibrarySupported ? 1 : 0));

        return isLibrarySupported;
    }

    public bool LoadOpenCV(ReadOnlyTargetRules Target)
    {
        // Start OpenCV linking here!
        bool isLibrarySupported = false;

        // Create OpenCV Path 
        string OpenCVPath = Path.Combine(ThirdPartyPath, "OpenCV");

        // Get Library Path 
        string LibPath = "";
        bool isdebug = Target.Configuration == UnrealTargetConfiguration.Debug && BuildConfiguration.bDebugBuildsActuallyUseDebugCRT;
        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            LibPath = Path.Combine(OpenCVPath, "Libraries", "Win64");
            isLibrarySupported = true;
        }
        else
        {
            string Err = string.Format("{0} dedicated server is made to depend on {1}. We want to avoid this, please correct module dependencies.", Target.Platform.ToString(), this.ToString()); System.Console.WriteLine(Err);
        }

        if (isLibrarySupported)
        {
            //Add Include path 
            PublicIncludePaths.AddRange(new string[] { Path.Combine(OpenCVPath, "Includes") });

            //PublicIncludePaths.AddRange(new string[] { @"C:\opencv\include" });
            //PublicIncludePaths.AddRange(new string[] { @"C:\dev\ros2\install\include" });
            //PublicIncludePaths.AddRange(new string[] { @"C:\Users\unreal\Documents\Visual Studio 2015\Projects\ROS2ImagePublisher\ROS2ImagePublisher" });

            // Add Library Path 
            PublicLibraryPaths.Add(LibPath);

            //Add Static Libraries
            PublicAdditionalLibraries.Add("opencv_world331.lib");

            //Add Dynamic Libraries
            PublicDelayLoadDLLs.Add("opencv_world331.dll");
            PublicDelayLoadDLLs.Add("opencv_ffmpeg331_64.dll");
        }

        Definitions.Add(string.Format("WITH_OPENCV_BINDING={0}", isLibrarySupported ? 1 : 0));

        return isLibrarySupported;
    }

    public CombiningComponents(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "RHI", "RenderCore", "ShaderCore", "PhysXVehicles" });

        LoadOpenCV(Target);

        LoadROSPublisher(Target);

        PrivateDependencyModuleNames.AddRange(new string[] {  });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
