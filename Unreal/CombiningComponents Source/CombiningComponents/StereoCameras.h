// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"	
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StereoCameras.generated.h"

class UCameraComponent;

UCLASS(config=Game)
class COMBININGCOMPONENTS_API AStereoCameras : public AActor
{
	GENERATED_BODY()
	
	/** Scene component for left camera view origin */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* LeftCameraBase;

	/** Scene component for right camera view origin */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* RightCameraBase;

	/** Camera component that will be our right viewpoint */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* RightCamera;

	/** Camera component that will be our left viewpoint */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* LeftCamera;
	
public:	
	// Sets default values for this actor's properties
	AStereoCameras();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
