// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"	
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

#include "Components/SceneCaptureComponent2D.h"
#include "Camera/CameraActor.h"
#include "Engine/TextureRenderTarget2D.h"
//#include "MyBluePrintFunctionLibrary.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OutputToOpenCVTest.generated.h"

UCLASS()
class COMBININGCOMPONENTS_API AOutputToOpenCVTest : public AActor
{
	GENERATED_BODY()

	/** Camera component that will be our right viewpoint */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneCaptureComponent2D* RightCamera;

	UTextureRenderTarget2D* textureTarget;
	FTextureRenderTargetResource* rt_resource;
	
public:	
	// Sets default values for this actor's properties
	AOutputToOpenCVTest();

	//Will hold an image that can be displayed by OpenCV
	cv::Mat image;

	void createAndDisplayImage();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void OutputImageToOpenCV(UTextureRenderTarget2D *texture2DRenderTarget);

	
	
};
