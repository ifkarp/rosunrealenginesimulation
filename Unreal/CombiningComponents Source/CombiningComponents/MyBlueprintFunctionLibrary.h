// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"	
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

#include "Components/SceneCaptureComponent2D.h"
#include "Camera/CameraActor.h"
#include "Engine/TextureRenderTarget2D.h"

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "MyBlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class UMyBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "ImageSaving")
	static void OutPutViewToOpenCV(UTextureRenderTarget2D *texture2DRenderTarget);

	//UFUNCTION(BlueprintCallable, Category = "ImageSaving")
	//static cv::Mat *getMatFromTextureRenderTarget(UTextureRenderTarget2D *texture2DRenderTarget);

	UFUNCTION(BlueprintCallable, Category = "ImageSaving")
	static void showStereoDepth(UTextureRenderTarget2D *textTargetLeft, UTextureRenderTarget2D *textTargetRight, float disparity);
	
};
