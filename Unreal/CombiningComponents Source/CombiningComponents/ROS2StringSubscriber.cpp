// Fill out your copyright notice in the Description page of Project Settings.

#include "ROS2StringSubscriber.h"
#include <iostream>


// Sets default values for this component's properties
UROS2StringSubscriber::UROS2StringSubscriber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UROS2StringSubscriber::BeginPlay()
{
	Super::BeginPlay();
	subscriber = new ROSStringSubscriber();
	Client = GEngine->GameViewport->Viewport->GetClient();
	owner = GetOwner();
	//TArray<UWheeledVehicleMovementComponent4W*> components;
	//owner->GetComponentsByClass<USceneCaptureComponent2D>(components);
	//owner->GetComponents(components);

	AWheeledVehicle* vehicle = (AWheeledVehicle*)owner;
	vehicleMovementComponent = ((AWheeledVehicle*)owner)->GetVehicleMovementComponent();
	Vehicle4W = CastChecked<UWheeledVehicleMovementComponent4W>(((AWheeledVehicle*)owner)->GetVehicleMovement());
	//auto Vehice4W = CastChecked<AWheeledVehicle>(vehicle->GetVehicleMovement());

	//for (UWheeledVehicleMovementComponent4W* comp : components)
	//{
	//	vehicleMovementComponent = comp->GetVehicleMovement();
	//}
	// ...
	
}


// Called every frame
void UROS2StringSubscriber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	subscriber->spin();
	std::string txt = subscriber->messageText;
	std::string up = "FORWARD";
	std::string right = "RIGHT";
	FString forward(up.c_str());
	FString Right(right.c_str());
	FString text(txt.c_str());
	UE_LOG(LogTemp, Warning, TEXT("%s"), *text);
	if (!(text.Compare(forward)))
	{
		Client->InputKey(GEngine->GameViewport->Viewport, 0, EKeys::Up, EInputEvent::IE_Pressed);
	}
	else if (!(text.Compare(Right)))
	{
		Client->InputKey(GEngine->GameViewport->Viewport, 0, EKeys::Right, EInputEvent::IE_Pressed);
	}
	

	//FRotator R = owner->GetActorRotation();
	//FVector V = R.UnrotateVector(owner->GetVelocity());

	//UE_LOG(LogTemp, Warning, TEXT("Velocity: %f, V4X: %f, V4Y: %f, V4Z: %f, VMX: %f, VMY: %f, VMZ: %f"), V.X, Vehicle4W->GetForwardSpeed(), Vehicle4W->Velocity.Y, Vehicle4W->Velocity.Z, vehicleMovementComponent->GetForwardSpeed(), vehicleMovementComponent->Velocity.Y, vehicleMovementComponent->Velocity.Z);
	//if (abs(V.X) < 500.0)
	//{
	//	//UE_LOG(LogTemp, Warning, TEXT("HITTING THROTTLE"));
	//	//vehicleMovementComponent->SetThrottleInput(1.0);
	//	//((AWheeledVehicle*)owner)->GetVehicleMovementComponent()->SetThrottleInput(1.0);
	//	Vehicle4W->SetThrottleInput(.2);
	//	Vehicle4W->SetSteeringInput(.2);
	//	//Vehicle4W
	//	/*Client->InputKey(GEngine->GameViewport->Viewport, 0, EKeys::Up, EInputEvent::IE_Pressed, MAX(0, 100 - abs(velocity)));*/
	//	//Client->InputKey(GEngine->GameViewport->Viewport, 0, EKeys::Up, EInputEvent::IE_Pressed, MAX(0, (1 - ((abs(V.X) / 2 + 1) / 300) * ((abs(V.X) / 2 + 1) / 300))));
	//}
	//else
	//{
	//	//Client->InputKey(GEngine->GameViewport->Viewport, 0, EKeys::Up, EInputEvent::IE_Released);
	//	//vehicleMovementComponent->SetThrottleInput(0);
	//	//Vehicle4W->SetThrottleInput(0.0);
	//	
	//	//Client->InputKey(GEngine->GameViewport->Viewport, 0, EKeys::Up, EInputEvent::IE_Released);
	//	UE_LOG(LogTemp, Warning, TEXT("HIT ELSE"));
	//}
}

