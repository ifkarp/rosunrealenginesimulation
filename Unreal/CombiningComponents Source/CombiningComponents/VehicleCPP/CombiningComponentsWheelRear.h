// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "CombiningComponentsWheelRear.generated.h"

UCLASS()
class UCombiningComponentsWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UCombiningComponentsWheelRear();
};



