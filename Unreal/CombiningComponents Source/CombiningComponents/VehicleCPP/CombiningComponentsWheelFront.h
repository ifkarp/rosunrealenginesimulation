// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "CombiningComponentsWheelFront.generated.h"

UCLASS()
class UCombiningComponentsWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UCombiningComponentsWheelFront();
};



