// Fill out your copyright notice in the Description page of Project Settings.

#include "MyBlueprintFunctionLibrary.h"

void UMyBlueprintFunctionLibrary::OutPutViewToOpenCV(UTextureRenderTarget2D *texture2DRenderTarget)
{
	TArray<FColor> TexturePixels;
	FTextureRenderTargetResource * fTextureRenderTargetRes = texture2DRenderTarget->GameThread_GetRenderTargetResource();
	int TexWidth = texture2DRenderTarget->SizeX;
	int TexHeight = texture2DRenderTarget->SizeY;
	int ImageSize = TexWidth * TexHeight;
	TexturePixels.AddUninitialized(ImageSize);

	cv::Mat mat(TexHeight, TexWidth, CV_8UC3);

	fTextureRenderTargetRes->ReadPixels(TexturePixels);
	int index = 0;
	for (int i = 0; i < TexHeight; ++i)
	{
		cv::Vec3b* pixel = mat.ptr<cv::Vec3b>(i);

		for (int j = 0; j < TexWidth; ++j)
		{
			index = i*TexWidth + j;
			pixel[j][2] = TexturePixels[index].R;
			pixel[j][1] = TexturePixels[index].G;
			pixel[j][0] = TexturePixels[index].B;
		}
	}

	cv::imshow("Display Window", mat);
}

void UMyBlueprintFunctionLibrary::showStereoDepth(UTextureRenderTarget2D *textTargetLeft, UTextureRenderTarget2D *textTargetRight, float disparity)
{
	//First get the image from camera left
	TArray<FColor> TexturePixels;
	FTextureRenderTargetResource * fTextureRenderTargetRes = textTargetLeft->GameThread_GetRenderTargetResource();
	int TexWidth = textTargetLeft->SizeX;
	int TexHeight = textTargetLeft->SizeY;
	int ImageSize = TexWidth * TexHeight;
	TexturePixels.AddUninitialized(ImageSize);

	cv::Mat matleft(TexHeight, TexWidth, CV_8UC3);

	fTextureRenderTargetRes->ReadPixels(TexturePixels);
	int index = 0;
	for (int i = 0; i < TexHeight; ++i)
	{
		cv::Vec3b* pixel = matleft.ptr<cv::Vec3b>(i);

		for (int j = 0; j < TexWidth; ++j)
		{
			index = i*TexWidth + j;
			pixel[j][2] = TexturePixels[index].R;
			pixel[j][1] = TexturePixels[index].G;
			pixel[j][0] = TexturePixels[index].B;
		}
	}


	//Get image from camera right
	fTextureRenderTargetRes = textTargetRight->GameThread_GetRenderTargetResource();
	TexWidth = textTargetRight->SizeX;
	TexHeight = textTargetRight->SizeY;
	ImageSize = TexWidth * TexHeight;
	TexturePixels.AddUninitialized(ImageSize);

	cv::Mat matright(TexHeight, TexWidth, CV_8UC3);

	fTextureRenderTargetRes->ReadPixels(TexturePixels);
	index = 0;
	for (int i = 0; i < TexHeight; ++i)
	{
		cv::Vec3b* pixel = matright.ptr<cv::Vec3b>(i);

		for (int j = 0; j < TexWidth; ++j)
		{
			index = i*TexWidth + j;
			pixel[j][2] = TexturePixels[index].R;
			pixel[j][1] = TexturePixels[index].G;
			pixel[j][0] = TexturePixels[index].B;
		}
	}
}


