// UnrealStandin.cpp : Defines the entry point for the console application.
//

#include <windows.h>
#include <stdlib.h> 
#include "stdafx.h"
#include "ROS2ImagePublisher.h"

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"	
#include "opencv2/imgproc.hpp"

//This project is used to make sure the ROS2ImagePublisher DLL works properly in the simplest possible environment

//Currently set up to use the ROSPublisher PublishTest() for random image messages
int main(int argc, char * argv[])
{
	//int t = test(argc, argv);
	//ROSStringPublisher strpublisher = ROSStringPublisher();
	//ROSStringSubscriber strsub = ROSStringSubscriber();
	//ROSLaserScanPublisher laserPub = ROSLaserScanPublisher(-135, 135, 2, 2, 1000);
	//printf("argc = %d", argc);
	//printf("argv = %s", argv);
	//int waitTime = 1000;

	//while (1)
	//{
	//	//strsub.spin();
	//	double ranges[(135 + 135) / 2];

	//	for (int i = 0; i < 270 / 2; i++)
	//	{
	//		ranges[i] = i;
	//	}

	//	laserPub.PublishScan(ranges, .5);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	//}

	ROSPublisher publisher = ROSPublisher(argc, argv);

	while (1)
	{
		/*cv::Mat mat(100, 100, CV_8UC1);
		double mean = 0.0;
		double stddev = 500.0 / 3.0;

		cv::randn(mat, cv::Scalar(mean), cv::Scalar(stddev));*/

		publisher.PublishTest();
	}
    return 0;
}

