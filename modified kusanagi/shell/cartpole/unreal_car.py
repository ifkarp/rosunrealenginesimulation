# pylint: disable=C0103
'''
Contains the Cartpole envionment, along with default parameters and
a rendering class
'''
import numpy as np
import math
from gym import spaces
from matplotlib import pyplot as plt

from kusanagi.shell import plant
from kusanagi.shell import cost
from kusanagi import utils

import rclpy
from std_msgs.msg import String
from sensor_msgs.msg import LaserScan
from rclpy.qos import qos_profile_default, qos_profile_sensor_data
from rclpy.qos import QoSReliabilityPolicy, QoSHistoryPolicy

class CommandPublisher(rclpy.Node):
    def __init__(self):
        super().__init__('command_pub')
        self.pub = self.create_publisher(String, 'chatter')

 #       rmw_qos_profile_t custom_qos_profile = rmw_qos_profile_default;
	#custom_qos_profile.depth = 7;
	#rmw_qos_history_policy_t history_policy = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
	#custom_qos_profile.history = history_policy;
        custom_qos_profile = qos_profile_default
        history_policy = QoSHistoryPolicy(1)
        custom_qos_profile.history = history_policy

        self.sub = self.create_subscription(LaserScan, 'unreal_LIDAR', self.chatter_callback, qos_profile=custom_qos_profile)
        assert self.sub  # prevent unused warning
        self.states = []
        self.inputs = []
        self.costs = []
        self.steering_angle = 0
        self.unreal_simulation_time_sec = -1
        self.unreal_simulation_time_nanosec = -1

    def publish_instruction(self, instruction):
        msg = String()
        msg.data = instruction
        print('Publishing instruction: ' + str(msg.data))
        self.pub.publish(msg)

    def chatter_callback(self, msg):
        response = 'Got it'
        #print(msg.ranges)
        ranges = msg.ranges
        left_range = np.mean(ranges[0:60])
        center_range = np.mean(ranges[60:75])
        right_range = np.mean(ranges[75:135])
        self.steering_angle = float(msg.intensities[0])
        x_t = [left_range, center_range, right_range]
        u_t = [self.steering_angle]
        c_t = ((msg.range_max - left_range) + (msg.range_max - center_range) + (msg.range_max - right_range))
        self.states.append(x_t)
        self.inputs.append(u_t)
        self.costs.append(c_t)
        self.unreal_simulation_time_sec = msg.header.stamp.sec + (msg.header.stamp.nanosec / math.pow(10,9))
        self.unreal_simulation_time_nanosec = msg.header.stamp.nanosec
        #x_t, u_t, c_t

class UnrealPlant(plant.Plant):
    def __init__(self, dt=0.1, noise_dist=None,
                 angle_dims=[], name='Plant',
                 *args, **kwargs):
        self.name = name
        self.dt = .1
        self.noise_dist = noise_dist
        self.angle_dims = angle_dims
        self.state = None
        self.u = None
        self.t = 0
        self.done = False
        self.renderer = None

        # initialize loss_func
        self.loss_func = None

        try:
            rclpy.init(args=args)
        except:
            print("rclpy.init already called")

        self.node = CommandPublisher()

    def set_state(self, state):
        self.state = np.array(state).flatten()

    def _step(self, action):
        if action is None: #If not getting an action, then user is sending input from a controller
            rclpy.spin_once(self.node) #Listen for one LIDARScan
            self.t = self.node.unreal_simulation_time_sec
            #print("Starting Time: " + str(self.t))
            t_end = self.t + self.dt
            #print(t_end)
            #print("About to start the spinning")
            done = False
            while self.t < t_end and not done:
                #print(self.t)
                #print("Waiting for the spin")
                rclpy.spin_once(self.node) #Listen for one LIDARScan
                self.t = self.node.unreal_simulation_time_sec #t = unreal_backend.get_current_simulation_time()
                if (np.sum(self.node.states[-1]) == 0): # done = unreal_backend.did_the_simulation_crash_explode_or_die()
                    done = True
                else:
                    done = False
                #time.sleep(.01)  # time.sleep(some_small_number) May be unnecessary 
           # print("Ending Time:" + str(self.t))
            self.state = np.array(self.node.states[-1]).flatten()  #state = unreal_backend.read_state()
            self.t = self.node.unreal_simulation_time_sec  #     self.t = unreal_backend.get_current_simulation_time()
            cost = self.node.costs[-1]  # cost = self.get_whatever_task_cost_you_want(state, action) # only needed for PILCO, can be None
            info = {}
            info["steering"] = self.node.steering_angle  #     info = unreal_backend._anything_that_you_would_like_to_know_from_the_simulation_aside_from_state()
            return self.state, cost, done, info  #     return self.state, cost, done, info
        else:
            self.node.publish_instruction(str(action[0])) # unreal_backend.send_command(action)
            rclpy.spin_once(self.node) #Listen for one LIDARScan
            self.t = self.node.unreal_simulation_time_sec
            #print(self.t)
            t_end = self.t + self.dt
            #print(t_end)
            #print("About to start the spinning")
            #print("Starting Time: " + str(self.t))
            done = False
            while self.t < t_end and not done:
                #print(self.t)
                #print("Waiting for the spin")
                rclpy.spin_once(self.node) #Listen for one LIDARScan
                self.t = self.node.unreal_simulation_time_sec #t = unreal_backend.get_current_simulation_time()
                if (np.sum(self.node.states[-1]) == 0): # done = unreal_backend.did_the_simulation_crash_explode_or_die()
                    done = True
                else:
                    done = False
                #time.sleep(.01)  # time.sleep(some_small_number) May be unnecessary 
            self.state = np.array(self.node.states[-1]).flatten()  #state = unreal_backend.read_state()
            self.t = self.node.unreal_simulation_time_sec  #     self.t = unreal_backend.get_current_simulation_time()
            #print("Ending Time:" + str(self.t))
            cost = self.node.costs[-1]  # cost = self.get_whatever_task_cost_you_want(state, action) # only needed for PILCO, can be None
            info = {}  #     info = unreal_backend._anything_that_you_would_like_to_know_from_the_simulation_aside_from_state()
            return self.state, cost, done, info  #     return self.state, cost, done, info

    def _reset(self):
        self.node.publish_instruction("RESET")
        rclpy.spin_once(self.node)
        self.set_state(self.node.states[-1])
        return np.array(self.state).flatten()

    def _render(self, mode='human', close=False):
        print("Hit render()")
        #if self.renderer is None:
        #    self.renderer = CartpoleDraw(self)
        #    self.renderer.init_ui()
        #self.renderer.update(*self.get_state(noisy=False))

    def dynamics(self, *args, **kwargs):
        msg = "You need to implement self.dynamics in the UnrealPlant subclass."
        raise NotImplementedError(msg)

