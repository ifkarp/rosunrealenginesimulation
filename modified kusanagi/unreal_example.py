
# coding: utf-8

# In[1]:

#get_ipython().run_line_magic('matplotlib', 'tk')
import os
import sys
import numpy as np
import theano
import datetime
import lasagne

from kusanagi.ghost import control
from kusanagi.ghost import regression
from kusanagi.shell.cartpole.unreal_car import UnrealPlant # TODO import unreal env (Plant subclass)
from kusanagi.ghost.algorithms import mc_pilco
from kusanagi.ghost.optimizers import SGDOptimizer
from kusanagi.base import apply_controller, train_dynamics, ExperienceDataset, apply_user_controller
from kusanagi import utils
from functools import partial
from matplotlib import pyplot as plt

# np.random.seed(1337)
np.set_printoptions(linewidth=500)


# In[2]:


# setup output directory
utils.set_output_dir(os.path.join(utils.get_output_dir(), 'unreal_env_experiments'))

n_user = 4
n_rnd = 2
n_apply = 3


# In[3]:


# init environment
env = UnrealPlant() ## TODO

# init policy (you need to obtain the state and control dimensions from the env)
# m0 = np.zeros([state dimension, control dimension])
# m0 = np.zeros([3])
m0 = np.zeros([3])
maxU = np.ones([1]) # example maximum steering angle, maximum gas, etc
pol = control.NNPolicy(m0, maxU=maxU)
# this specifies the network architecture (weird way that I use)
pol_spec = regression.mlp(
                input_dims=pol.D,
                output_dims=pol.E,
                hidden_dims=[100]*2,
                nonlinearities=regression.nonlinearities.rectify,
                W_init=lasagne.init.GlorotNormal(),
                output_nonlinearity=pol.sat_func,
                name='NN_policy')
# this builds the computation graph
pol.network = pol.build_network(pol_spec)
# this is just a random controller
randpol = control.RandPolicy(maxU=pol.maxU)

# init dynmodel (This is only needed for PILCO)
#dyn = regression.BNN(state_dimensions+control_dimensions, state_dimensions)
# you can specify the network architecture similaar to the policy example above

# init cost model TODO this is only needed if you are running PILCO. you need to provide an analytic expression for the reward
#cost = partial(cartpole.cartpole_loss, **params['cost'])

# create experience dataset. This is the data structure that will be populated with data from unreal, 
# when calling apply_controller
exp = ExperienceDataset()


# In[6]:


# this is the number of steps to run the simulation for (for colelcting data with a gamepad, set it to something large and use 
# the 'done' flag to stop collecting data )
H = 450  #H = time_in_seconds/control_rate

# callback executed after every call to env.step. will be useful for debugging  the data coming from unreal
# in this case, this is how we populate the ExperienceDataset
def step_cb(state, action, cost, info):
    # add new data (single step)
    exp.add_sample(state, action, cost, info)
    # this  could be used to tell unreal to do something. You can make it an empty function by default
    #env.render()

for i in range(n_user):
    # tell the experience dataset we are collecting data from a new episode
    exp.new_episode()
    # run the random controller for H steps
    apply_user_controller(env, None, H,
                     callback=step_cb)


# during first n_rnd trials, apply randomized controls
#for i in range(n_rnd):
#    # tell the experience dataset we are collecting data from a new episode
#    exp.new_episode()
#    # run the random controller for H steps
#    apply_controller(env, randpol, H,
#                     callback=step_cb)
    
#for i in range(n_apply):
#    # tell the experience dataset we are collecting data for a new episode
#    exp.new_episode()
#    # run the controller for H steps
#    apply_controller(env, pol, H,
#                     callback=step_cb)
    
# If you wanted to collect data with a gamepad, you could do something similar,
# where you run apply controller for a large number of timesteps. You could create a new controller (similar to RandPolicy)
# where the controls come from the gamepad.

# inside the step function of your unreal environment, you should control the rate at which commands are sent to the simulation
# for example
# def _step(self, action):
#     unreal_backend.send_command(action)
#     t_end = self.t + dt
#     while t < t_end:
#         t = unreal_backend.get_current_simulation_time()
#         time.sleep(some_small_number)
#     state = unreal_backend.read_state()
#     self.t = unreal_backend.get_current_simulation_time()
#     cost = self.get_whatever_task_cost_you_want(state, action) # only needed for PILCO, can be None
#     done = unreal_backend.did_the_simulation_crash_explode_or_die()
#     info = unreal_backend._anything_that_you_would_like_to_know_from_the_simulation_aside_from_state()
#     return self.state, cost, done, info
#
# the unreal_backend would take care of all the (possibly asynchronous) communications with the simulation. The 
# Plant class would deal with presenting the data to learrning algorithms in a synchronous manner


# In[5]:


# once you've collected data in the experience dataset class
X, Y = exp.get_dynmodel_dataset() # convert states into a matrix of samplesxstate_dimensions
# the data data we want is containted in the X matrix
#states = X[:state_dimensions]
#actions = X[state_dimensions:control_dimensions]
states = X[:,:3]
actions = X[:,3:]


# set the dataset to fit with the neural net policy
pol.set_dataset(states, actions)
# train tthe policy (batch_Size is how many examples to present to the network at every optimization step, lr is the learning rate)
pol.train(batch_size=25, lr=1e-4)
#pol.save(output_folder, 'policy')

H = 100000

for i in range(n_apply):
    # tell the experience dataset we are collecting data for a new episode
    #exp.new_episode()
    # run the controller for H steps
    print(datetime.datetime.now())
    apply_controller(env, pol, H,
                     callback=step_cb)
    print(datetime.datetime.now())

# now we can try applying the nerual net policy
# run the controller for H steps
#apply_controller(env, pol, H)
